// Test class


#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "Thread.h"

TEST_CASE("PLRU Table") {
    int associativity = 4;
    BinaryTree* t = new BinaryTree(associativity);
    // Content should be     A B C D with 0 everywhere
    // Translated it will be 0 1 2 3
    t->setMRU(2);
    t->setMRU(0);
    t->setMRU(1); // B
    CHECK(t->getPLRU()== 3);// PLRU => D -> 3
    t->setMRU(2); // C
    CHECK(t->getPLRU()== 0);// PLRU => A -> 0
    t->setMRU(0); // A
    CHECK(t->getPLRU()== 3);// PLRU => D -> 3
    t->setMRU(3); // D
    CHECK(t->getPLRU()== 1);// PLRU => B -> 1
    t->setMRU(t->getPLRU()); // E
    CHECK(t->getPLRU()== 2);// PLRU => C -> 1
    
}

TEST_CASE("Debug 1 - L1 - LRU") {
    Thread* t = new Thread();
    t->print = false;
    t->unit_test = true;
    t->setArgumentsTest(64, 32768, 8, 0, 0, LRU, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 85152);
    CHECK(t->stats[L1_CACHE]->reads_miss == 22865);
    CHECK(t->stats[L1_CACHE]->writes == 14848);
    CHECK(t->stats[L1_CACHE]->writes_miss == 3906);
    CHECK(t->stats[L1_CACHE]->writes_back == 10324);
}

TEST_CASE("Debug 2 - L1 - FIFO") {
    Thread* t = new Thread();
    t->print = false;
    t->unit_test = true;
    t->setArgumentsTest(64, 32768, 8, 0, 0, FIFO, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 85152);
    CHECK(t->stats[L1_CACHE]->reads_miss == 22876);
    CHECK(t->stats[L1_CACHE]->writes == 14848);
    CHECK(t->stats[L1_CACHE]->writes_miss == 3910);
    CHECK(t->stats[L1_CACHE]->writes_back == 10346);
}


TEST_CASE("Debug 3 - L1 - PLRU") {
    Thread* t = new Thread();
    t->print = false;
    t->unit_test = true;
    t->setArgumentsTest(64, 32768, 8, 0, 0, PLRU, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 85152);
    CHECK(t->stats[L1_CACHE]->reads_miss == 22873);
    CHECK(t->stats[L1_CACHE]->writes == 14848);
    CHECK(t->stats[L1_CACHE]->writes_miss == 3906);
    CHECK(t->stats[L1_CACHE]->writes_back == 10319);
}


TEST_CASE("Debug 4 - L2 - LRU - NINE") {
    Thread* t = new Thread();
    t->print = false;
    t->unit_test = true;
    t->setArgumentsTest(64, 32768, 8, 524288, 8, LRU, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 85152);
    CHECK(t->stats[L1_CACHE]->reads_miss == 22865);
    CHECK(t->stats[L1_CACHE]->writes == 14848);
    CHECK(t->stats[L1_CACHE]->writes_miss == 3906);
    CHECK(t->stats[L1_CACHE]->writes_back == 10324);
    CHECK(t->stats[L2_CACHE]->reads == 26771);
    CHECK(t->stats[L2_CACHE]->reads_miss == 21303);
    CHECK(t->stats[L2_CACHE]->writes == 10324);
    CHECK(t->stats[L2_CACHE]->writes_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes_back == 5857);

}

TEST_CASE("Debug 5 - L2 - LRU - INCLUSIVE") {
    Thread* t = new Thread();
    t->print = false;
    t->unit_test = true;
    t->setArgumentsTest(64, 32768, 8, 524288, 8, LRU, INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 85152);
    CHECK(t->stats[L1_CACHE]->reads_miss == 22865);
    CHECK(t->stats[L1_CACHE]->writes == 14848);
    CHECK(t->stats[L1_CACHE]->writes_miss == 3906);
    CHECK(t->stats[L1_CACHE]->writes_back == 10324);
    CHECK(t->stats[L2_CACHE]->reads == 26771);
    CHECK(t->stats[L2_CACHE]->reads_miss == 21303);
    CHECK(t->stats[L2_CACHE]->writes == 10324);
    CHECK(t->stats[L2_CACHE]->writes_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes_back == 5857);
    
}


TEST_CASE("Debug 6 - L2 - LRU - EXCLUSIVE") {
    Thread* t = new Thread();
    t->print = false;
    t->unit_test = true;
    t->setArgumentsTest(64, 32768, 2, 262144, 4, LRU, EXCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 85152);
    CHECK(t->stats[L1_CACHE]->reads_miss == 23157);
    CHECK(t->stats[L1_CACHE]->writes == 14848);
    CHECK(t->stats[L1_CACHE]->writes_miss == 3952);
    CHECK(t->stats[L1_CACHE]->writes_back == 11250);
    CHECK(t->stats[L2_CACHE]->reads == 27109);
    CHECK(t->stats[L2_CACHE]->reads_miss == 23862);
    CHECK(t->stats[L2_CACHE]->writes == 26597);
    CHECK(t->stats[L2_CACHE]->writes_miss == 26597);
    CHECK(t->stats[L2_CACHE]->writes_back == 8254);
    
}


// Validations from here on
TEST_CASE("Validation 0") {
    Thread* t = new Thread();
    t->print = false;
    t->validation = true;
    t->setArgumentsTest(64, 32768, 4, 0, 0, LRU, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 1701892);
    CHECK(t->stats[L1_CACHE]->reads_miss == 449851);
    CHECK(t->stats[L1_CACHE]->writes == 298108);
    CHECK(t->stats[L1_CACHE]->writes_miss == 79039);
    CHECK(t->stats[L1_CACHE]->writes_back == 198448);
    CHECK(t->stats[L2_CACHE]->reads == 0);
    CHECK(t->stats[L2_CACHE]->reads_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes == 0);
    CHECK(t->stats[L2_CACHE]->writes_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes_back == 0);
    
}

TEST_CASE("Validation 1") {
    Thread* t = new Thread();
    t->print = false;
    t->validation = true;
    t->setArgumentsTest(64, 65536, 8, 0, 0, LRU, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 1701892);
    CHECK(t->stats[L1_CACHE]->reads_miss == 419361);
    CHECK(t->stats[L1_CACHE]->writes == 298108);
    CHECK(t->stats[L1_CACHE]->writes_miss == 73841);
    CHECK(t->stats[L1_CACHE]->writes_back == 188626);
    CHECK(t->stats[L2_CACHE]->reads == 0);
    CHECK(t->stats[L2_CACHE]->reads_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes == 0);
    CHECK(t->stats[L2_CACHE]->writes_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes_back == 0);
    
}

TEST_CASE("Validation 2") {
    Thread* t = new Thread();
    t->print = false;
    t->validation = true;
    t->setArgumentsTest(64, 65536, 8, 0, 0, FIFO, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 1701892);
    CHECK(t->stats[L1_CACHE]->reads_miss == 424688);
    CHECK(t->stats[L1_CACHE]->writes == 298108);
    CHECK(t->stats[L1_CACHE]->writes_miss == 74754);
    CHECK(t->stats[L1_CACHE]->writes_back == 192804);
    CHECK(t->stats[L2_CACHE]->reads == 0);
    CHECK(t->stats[L2_CACHE]->reads_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes == 0);
    CHECK(t->stats[L2_CACHE]->writes_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes_back == 0);
    
}


TEST_CASE("Validation 3") {
    Thread* t = new Thread();
    t->print = false;
    t->validation = true;
    t->setArgumentsTest(64, 32768, 4, 0, 0, PLRU, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 1701892);
    CHECK(t->stats[L1_CACHE]->reads_miss == 449918);
    CHECK(t->stats[L1_CACHE]->writes == 298108);
    CHECK(t->stats[L1_CACHE]->writes_miss == 79067);
    CHECK(t->stats[L1_CACHE]->writes_back == 198494);
    CHECK(t->stats[L2_CACHE]->reads == 0);
    CHECK(t->stats[L2_CACHE]->reads_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes == 0);
    CHECK(t->stats[L2_CACHE]->writes_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes_back == 0);
}


TEST_CASE("Validation 4") {
    Thread* t = new Thread();
    t->print = false;
    t->validation = true;
    t->setArgumentsTest(64, 32768, 4, 262144, 4, LRU, NON_INCLUSIVE, "LBM.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 1573624);
    CHECK(t->stats[L1_CACHE]->reads_miss == 170813);
    CHECK(t->stats[L1_CACHE]->writes == 426376);
    CHECK(t->stats[L1_CACHE]->writes_miss == 44628);
    CHECK(t->stats[L1_CACHE]->writes_back == 150821);
    CHECK(t->stats[L2_CACHE]->reads == 215441);
    CHECK(t->stats[L2_CACHE]->reads_miss == 131876);
    CHECK(t->stats[L2_CACHE]->writes == 150821);
    CHECK(t->stats[L2_CACHE]->writes_miss == 18);
    CHECK(t->stats[L2_CACHE]->writes_back == 104239);
    
}

TEST_CASE("Validation 5") {
    Thread* t = new Thread();
    t->print = false;
    t->validation = true;
    t->setArgumentsTest(64, 32768, 1, 262144, 4, LRU, NON_INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 1701892);
    CHECK(t->stats[L1_CACHE]->reads_miss == 457564);
    CHECK(t->stats[L1_CACHE]->writes == 298108);
    CHECK(t->stats[L1_CACHE]->writes_miss == 80404);
    CHECK(t->stats[L1_CACHE]->writes_back == 201098);
    CHECK(t->stats[L2_CACHE]->reads == 537968);
    CHECK(t->stats[L2_CACHE]->reads_miss == 422235);
    CHECK(t->stats[L2_CACHE]->writes == 201098);
    CHECK(t->stats[L2_CACHE]->writes_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes_back == 166551);
    
}


TEST_CASE("Validation 6") {
    Thread* t = new Thread();
    t->print = false;
    t->validation = true;
    t->setArgumentsTest(64, 32768, 2, 262144, 4, LRU, EXCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 1701892);
    CHECK(t->stats[L1_CACHE]->reads_miss == 450895);
    CHECK(t->stats[L1_CACHE]->writes == 298108);
    CHECK(t->stats[L1_CACHE]->writes_miss == 79174);
    CHECK(t->stats[L1_CACHE]->writes_back == 234353);
    CHECK(t->stats[L2_CACHE]->reads == 530069);
    CHECK(t->stats[L2_CACHE]->reads_miss == 420110);
    CHECK(t->stats[L2_CACHE]->writes == 529557);
    CHECK(t->stats[L2_CACHE]->writes_miss == 529557);
    CHECK(t->stats[L2_CACHE]->writes_back == 165816);
}

TEST_CASE("Validation 7") {
    Thread* t = new Thread();
    t->print = false;
    t->validation = true;
    t->setArgumentsTest(64, 32768, 2, 262144, 4, LRU, INCLUSIVE, "MCF.t");
    t->start();
    CHECK(t->stats[L1_CACHE]->reads == 1701892);
    CHECK(t->stats[L1_CACHE]->reads_miss == 450909);
    CHECK(t->stats[L1_CACHE]->writes == 298108);
    CHECK(t->stats[L1_CACHE]->writes_miss == 79178);
    CHECK(t->stats[L1_CACHE]->writes_back == 198973);
    CHECK(t->stats[L2_CACHE]->reads == 530087);
    CHECK(t->stats[L2_CACHE]->reads_miss == 422078);
    CHECK(t->stats[L2_CACHE]->writes == 198973);
    CHECK(t->stats[L2_CACHE]->writes_miss == 0);
    CHECK(t->stats[L2_CACHE]->writes_back == 166309);
    
}






