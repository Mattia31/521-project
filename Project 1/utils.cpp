//
//  utils.c
//  Project 1
//
//  Created by Mattia Muller on 2/11/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#include "utils.h"


/**
 Creates a new file (deletes the old one if necessary
 
 @param trace_file the trace file
 @return 0 if success, 1 if error
 */
int new_trace_file(std::string trace_file ){
    
    try{
        std::ofstream trace_file_stream;
        trace_file_stream.open (trace_file.c_str(), std::fstream::trunc);
        trace_file_stream.close();
        return 0;
    } catch (int e){
        std::cout << "There was an error in the print trace function error id: " << e;
        return 1;
    }
    
};


/**
 Appends a line to the trace file
 
 @param mode the char that defines if it is a read or write
 @param address the address of the read or write
 @param trace_file the trace file
 @return 0 if success, 1 if error
 */
int append_trace_to_file(char mode, uint64_t address, std::string trace_file ){
    
    try{
        std::ofstream trace_file_stream;
        trace_file_stream.open (trace_file.c_str(), std::fstream::app);
        trace_file_stream << mode << " " << std::hex << address << "\n";
        trace_file_stream.close();
        return 0;
    } catch (int e){
        std::cout << "There was an error in the print trace function error id: " << e;
        return 1;
    }
    
};





