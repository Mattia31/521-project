//
//  Thread.cpp
//  Project 1
//
//  Created by Mattia Muller on 3/6/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#include "Thread.h"

namespace arguments {
    uint64_t BLOCKSIZE, L1_SIZE, L1_ASSOC, L2_SIZE, L2_ASSOC;
    uint16_t REPLACEMENT_POLICY, INCLUSION_PROPERTY;
    std::string trace_file;
}


Thread::Thread(){}

void Thread::setArguments(const char **argv){
    arguments::BLOCKSIZE = atoi(argv[1]);
    arguments::L1_SIZE = atoi(argv[2]);
    arguments::L1_ASSOC = atoi(argv[3]);
    arguments::L2_SIZE = atoi(argv[4]);
    arguments::L2_ASSOC = atoi(argv[5]);
    arguments::REPLACEMENT_POLICY = atoi(argv[6]);
    arguments::INCLUSION_PROPERTY = atoi(argv[7]);
    arguments::trace_file =  argv[8];
}

void Thread::setArgumentsTest(uint64_t BLOCKSIZE,
                              uint64_t L1_SIZE,
                              uint64_t L1_ASSOC,
                              uint64_t L2_SIZE,
                              uint64_t L2_ASSOC,
                              uint16_t REPLACEMENT_POLICY,
                              uint16_t INCLUSION_PROPERTY,
                              std::string trace_file){
    arguments::BLOCKSIZE = BLOCKSIZE;
    arguments::L1_SIZE = L1_SIZE;
    arguments::L1_ASSOC = L1_ASSOC;
    arguments::L2_SIZE = L2_SIZE;
    arguments::L2_ASSOC = L2_ASSOC;
    arguments::REPLACEMENT_POLICY = REPLACEMENT_POLICY;
    arguments::INCLUSION_PROPERTY = INCLUSION_PROPERTY;
    arguments::trace_file =  trace_file;
}

void Thread::start(){
    bool print = true, unit_test = false, validation=false;
    char mode;
    char first = 0x01;
    uint64_t address;
    
    stats[0] = new Measurements();
    stats[1] = new Measurements();
    
    L1 = new Cache(arguments::BLOCKSIZE, arguments::L1_SIZE, arguments::L1_ASSOC, arguments::REPLACEMENT_POLICY, arguments::INCLUSION_PROPERTY, stats[L1_CACHE]);
    if (arguments::L2_SIZE > 0) {
        L2 = new Cache(arguments::BLOCKSIZE, arguments::L2_SIZE, arguments::L2_ASSOC, arguments::REPLACEMENT_POLICY, arguments::INCLUSION_PROPERTY, stats[L2_CACHE]);
        
        L1->set_next_cache(L2);
        L2->set_previous_cache(L1);
    }
    
    
    int debug_stuff = 0;
    
    std::ifstream infile(arguments::trace_file.c_str());
    while (infile >> mode >> std::hex >> address)// Loop through the file
    {
        //std::cout << mode << " " << std::hex << address << "\n";
        if (first) { // If ti is the first time we loop let's etup our address size
            L1->set_address_size(address);
            first = 0x00; // Ensure we do not do this anymore
        }
        //address = 0x00000000FFFFFFFF & address;
        switch (mode) {
            case 'r':
                L1->read(address);
                break;
            case 'w':
                L1->write(address, true);
                break;
            default:
                break;
        }
        
        if (validation)
            continue;
        
        
        
        debug_stuff++;
        //std::cout << mode << " " << std::hex << address << "\n";
        //stats[L1_CACHE]->print_measurements();
        uint64_t test = 299829;
        if (!unit_test && (debug_stuff == test || stats[L2_CACHE]->writes_miss == 1)) {
            int caca = 1;
        }
        if (unit_test && debug_stuff == 100000) {
            break;
            
        }
    }
    if (print){
        
        std::cout << "===== Simulator configuration =====\n";
        std::cout << "BLOCKSIZE:             " << std::dec << arguments::BLOCKSIZE << "\n";
        std::cout << "L1_SIZE:               " << std::dec << arguments::L1_SIZE << "\n";
        std::cout << "L1_ASSOC:              " << std::dec << arguments::L1_ASSOC << "\n";
        std::cout << "L2_SIZE:               " << std::dec << arguments::L2_SIZE << "\n";
        std::cout << "L2_ASSOC:              " << std::dec << arguments::L2_ASSOC << "\n";
        switch (arguments::REPLACEMENT_POLICY) {
            case LRU:
                std::cout << "REPLACEMENT POLICY:    LRU\n";
                break;
            case FIFO:
                std::cout << "REPLACEMENT POLICY:    FIFO\n";
                break;
            case PLRU:
                std::cout << "REPLACEMENT POLICY:    PLRU\n";
                break;
            default:
                break;
        }
        switch (arguments::INCLUSION_PROPERTY) {
            case NON_INCLUSIVE:
                std::cout << "INCLUSION PROPERTY:    non-inclusive\n";
                break;
            case INCLUSIVE:
                std::cout << "INCLUSION PROPERTY:    inclusive\n";
                break;
            case EXCLUSIVE:
                std::cout << "INCLUSION PROPERTY:    exclusive\n";
                break;
            default:
                break;
        }
        std::cout << "trace_file:            " << arguments::trace_file << "\n";
    
   
        std::cout << "===== Simulation results (raw) =====\n";
        std::cout << "a. number of L1 reads:        " << stats[L1_CACHE]->reads << "\n";
        std::cout << "b. number of L1 read misses:  " << stats[L1_CACHE]->reads_miss << "\n";
        std::cout << "c. number of L1 writes:       " << stats[L1_CACHE]->writes << "\n";
        std::cout << "d. number of L1 write misses: " << stats[L1_CACHE]->writes_miss << "\n";
        double missrate = (double)(stats[L1_CACHE]->reads_miss+stats[L1_CACHE]->writes_miss)/(stats[L1_CACHE]->reads+stats[L1_CACHE]->writes);
        std::cout << "e. L1 miss rate:              " << missrate << "\n";
        std::cout << "f. number of L1 writebacks:   " << stats[L1_CACHE]->writes_back << "\n";
        std::cout << "g. number of L2 reads:        " << stats[L2_CACHE]->reads << "\n";
        std::cout << "h. number of L2 read misses:  " << stats[L2_CACHE]->reads_miss << "\n";
        std::cout << "i. number of L2 writes:       " << stats[L2_CACHE]->writes << "\n";
        std::cout << "j. number of L2 write misses: " << stats[L2_CACHE]->writes_miss << "\n";
        missrate = (double)(stats[L2_CACHE]->reads_miss)/(stats[L2_CACHE]->reads);
        std::cout << "k. L2 miss rate:              " << missrate << "\n";
        std::cout << "l. number of L2 writebacks:   " << stats[L2_CACHE]->writes_back << "\n";
        uint64_t total_mem = 0;
        switch (arguments::INCLUSION_PROPERTY) {
            case NON_INCLUSIVE:
                total_mem = stats[L2_CACHE]->reads_miss + stats[L2_CACHE]->writes_miss + stats[L2_CACHE]->writes_back;
                break;
            case INCLUSIVE:
                total_mem = stats[L2_CACHE]->writes_miss + stats[L2_CACHE]->reads_miss + stats[L2_CACHE]->writes_back + stats[L2_CACHE]->back_invals;
                break;
            case EXCLUSIVE:
                total_mem = stats[L1_CACHE]->writes_back + stats[L2_CACHE]->reads_miss + stats[L2_CACHE]->writes_back;
                break;
            default:
                break;
        }
        std::cout << "m. total memory traffic:      " <<  total_mem << "\n";
        
    }

}