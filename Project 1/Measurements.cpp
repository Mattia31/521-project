//
//  Measurements.cpp
//  Project 1
//
//  Created by Mattia Muller on 2/12/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#include "Measurements.h"

Measurements::Measurements(){
    reads = 0;
    reads_miss = 0;
    writes = 0;
    writes_miss = 0;
    writes_back = 0;
    back_invals = 0;
};

int Measurements::print_measurements(){
    std::cout << "reads: " <<  std::dec << reads  << "\n" << " reads_miss: " <<  reads_miss << "\n";
    std::cout << "writes: " <<  writes << "\n" << " writes_miss: " <<  writes_miss  << "\n";
    std::cout << "writebacks: " <<  writes_back  << "\n";
    return 0;
}

void Measurements::read() {
    reads++;
}

void Measurements::read_miss() {
    reads_miss++;
}

void Measurements::back_inval() {
    back_invals++;
}


void Measurements::write() {
    writes++;
}

void Measurements::write_miss() {
    writes_miss++;
}

void Measurements::write_back() {
    writes_back++;
}

