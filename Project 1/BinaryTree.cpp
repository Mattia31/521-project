//
//  BinaryTree.cpp
//  Project 1
//
//  Created by Mattia Muller on 3/5/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#include "BinaryTree.h"

#define INIT_PLRU 1

BinaryTree::BinaryTree()
{
    root=NULL;
}

BinaryTree::BinaryTree(uint16_t depth_value)
{
    root=nullptr;
    set_depth(depth_value);
    
}

BinaryTree::~BinaryTree()
{
    destroy_tree();
}

void BinaryTree::create(){
    
    // We are gonna create one less level than required so that we can fill it up when the first entries arrive
    // lvl:1                0
    //                  /      \
    // lvl:2         1          2
    //             /   \      /   \
    // lvl:3      3     4    5     6
    // lvl:4     7  8   9 10 11 12 13 14
    //
    // lvl = log2(associativity)+1 => 1 => lvl:1 | 2 => lvl:2 | 4 => lvl:3 | 8 => lvl:4
    // to initialize it though we just want log2(associativity)
    uint16_t level = log2(associativity);
    
    // 1, 3, 7, 15
    // 2^n -1
    // save previous value
    //level = getKeyMax(level+1);
    
    //    for (uint16_t i = 0; i<level; i++){
    //        insert(i);
    //    }
    
    // init the table
    root=new node;
    root->zero=nullptr;
    root->one=nullptr;
    insert_pvt(level, root);
    for (uint16_t i = 0; i<associativity; i++){
        setMRU_pvt(i, associativity, root)->assoc = i;
    }
}


uint16_t BinaryTree::setPLRU(){
    uint16_t t = setPLRU_pvt(root)->assoc;
    return t;
}

uint16_t BinaryTree::getPLRU(){
    uint16_t t = getPLRU_pvt(root)->assoc;
    return t;
}

node* BinaryTree::setPLRU_pvt(node *leaf){
    // we need to go down and toggle the value
    if (leaf->one == nullptr || leaf->zero == nullptr)
        return leaf;
    
    if (leaf->lru == 0){
        leaf->lru = 1;
        return setPLRU_pvt(leaf->one);
    } else if (leaf->lru == 1){
        leaf->lru = 0;
        return setPLRU_pvt(leaf->zero);
    }
    
    return leaf;
}



node* BinaryTree::getPLRU_pvt(node *leaf){
    if (leaf->one == nullptr || leaf->zero == nullptr)
        return leaf;
    
    if (leaf->lru == 0){
        return getPLRU_pvt(leaf->one);
    } else if (leaf->lru == 1){
        return getPLRU_pvt(leaf->zero);
    }
    return leaf;
}



uint16_t BinaryTree::getKeyMax(uint16_t level){
    
    return pow(2,level)-1;
}


void BinaryTree::destroy_tree_pvt(node *leaf)
{
    if(leaf!=nullptr)
    {
        destroy_tree_pvt(leaf->zero);
        destroy_tree_pvt(leaf->one);
        delete leaf;
    }
}

void BinaryTree::set_depth(uint16_t depth_value)
{
    associativity = depth_value;
    if (root !=nullptr)
        destroy_tree_pvt(root);
    create();
}

void BinaryTree::insert_pvt(int depth, node *leaf)
{
    
    if (depth == 0){
        return;
    }
    
    depth--;
    leaf->zero=new node;
    leaf->one=new node;
    leaf->lru = INIT_PLRU;
    leaf->zero->zero=nullptr;    //Sets the left child of the child node to null
    leaf->zero->one=nullptr;
    leaf->one->zero=nullptr;    //Sets the left child of the child node to null
    leaf->one->one=nullptr;
    insert_pvt(depth, leaf->zero);
    insert_pvt(depth, leaf->one);
}

void BinaryTree::insert(int key)
{
    if(root!=nullptr)
        insert_pvt(key, root);
    else
    {
        
    }
}

void BinaryTree::destroy_tree()
{
    destroy_tree_pvt(root);
}


void BinaryTree::postorder(node* p, int indent=0)
{
    if(p != nullptr) {
        if(p->zero) postorder(p->zero, indent+4);
        if(p->one) postorder(p->one, indent+4);
        if (indent) {
            std::cout << std::setw(indent) << ' ';
        }
        std::cout<< p->lru << "\n ";
        
    }
}

void BinaryTree::print(int indent){
    printBinaryTree(root);
}

void BinaryTree::setMRU(int assoc){
    // ASSOC    0   4   2   6   1   5   3   7
    // INDEX    0   1   2   3   4   5   6   7
    setMRU_pvt(assoc, associativity, root);
    
}

node* BinaryTree::setMRU_pvt(int assoc, int level, node *leaf){
    
    if (level==1){
        return leaf;
    }
    
    if(leaf==nullptr)
        return leaf;
    if(leaf->zero==nullptr || leaf->one ==nullptr)
        return leaf;
    
    float left_or_right = (float)assoc/ (float)level; // assoc / (associativity)
    
    if (left_or_right < 0.5){
        leaf->lru = 0;
        return setMRU_pvt(assoc,level/2,leaf->zero);
    } else if (left_or_right >= 0.5) {
        leaf->lru = 1;
        return setMRU_pvt(assoc-(level/2), level/2, leaf->one);
    }
    
    return leaf;
    
}

node* BinaryTree::search(int assoc, node *leaf){
    
    if(leaf==nullptr)
        return leaf;
    
    if(leaf->zero==nullptr || leaf->one ==nullptr)
        return leaf;
    
    if(assoc==search(assoc, leaf->zero)->assoc){
        leaf->lru = 0;
        leaf->assoc = assoc;
        return leaf;
    }
    
    if(assoc==search(assoc, leaf->one)->assoc){
        leaf->lru = 1;
        leaf->assoc = assoc;
        return leaf;
    }
    
    
    return leaf;
}

void BinaryTree::printBinaryTree(node *n) {
    if (nullptr == n) {
        return;
    }
    int level = 0;
    
    // Use a queue for breadth-first traversal of the tree.  The pair is
    // to keep track of the depth of each node.  (Depth of root node is 1.)
    typedef std::pair<node*,int> node_level;
    std::queue<node_level> q;
    q.push(node_level(n, 1));
    
    while (!q.empty()) {
        node_level nl = q.front();
        q.pop();
        if (nullptr != (n = nl.first)) {
            if (level != nl.second) {
                std::cout << " Level " << nl.second << ": ";
                level = nl.second;
            }
            if (nl.second == (int) log2(associativity)+1){
                std::cout << "[" << n->assoc << "]" << ' ';
            } else
                std::cout << n->lru << ' ';
            
            q.push(node_level(n->zero,  1 + level));
            q.push(node_level(n->one, 1 + level));
        }
    }
    std::cout << std::endl;
}




