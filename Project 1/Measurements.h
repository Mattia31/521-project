//
//  Measurements.h
//  Project 1
//
//  Created by Mattia Muller on 2/12/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#ifndef __Project_1__Measurements__
#define __Project_1__Measurements__

#include <stdint.h>
#include <iostream>

class Measurements{
//private:
    
public:
    uint64_t reads, reads_miss, writes, writes_miss, writes_back, back_invals;
    Measurements();
    int print_measurements();
    void read();
    void read_miss();
    void write();
    void write_miss();
    void write_back();
    void back_inval();
};

#endif /* defined(__Project_1__Measurements__) */
