//
//  cache.h
//  Project 1
//
//  Created by Mattia Muller on 2/12/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#ifndef __Project_1__cache__
#define __Project_1__cache__


// REPLACEMENT_POLICY:
#define LRU     0
#define FIFO    1
#define PLRU    2 // PSEUDO-LRU

// INCLUSION_PROPERTY:
#define NON_INCLUSIVE   0
#define INCLUSIVE       1
#define EXCLUSIVE       2

#define MAX_INT_64      0xffffffffffffffff

#include <stdint.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <math.h>
#include "DynamicArray.h"
#include "Measurements.h"
#include "BinaryTree.h"
#include "utils.h"
#include <algorithm> 

#define CALL_MEMBER_FN(ptrToMember)  ((this->*(ptrToMember)))

enum last_action {
    READ,
    READ_MISS,
    WRITE,
    WRITE_MISS
};

class Cache{
private:
    std::string my_cache_name;
    Measurements* stat_keeper;
    DynamicArray<uint64_t>  cache_table;
    DynamicArray<uint64_t>  dirty_table;  // Contains the dirty bits
    DynamicArray<uint64_t>  time_table;  // Contains the timestamp
    std::vector<BinaryTree*>  tree_table;
    uint64_t    tag_mask, index_mask;
    
    class Propreties{
    public:
        uint64_t size, associativity, block_size, index_size;
        uint16_t replacement_policy, inclusion_proprety, address_size, tag_size, offset_size;
        Cache* previousCache;
        Cache* nextCache;
    };
    Propreties p;
    
    uint64_t time;
    
    last_action last_state;
    
    uint64_t lru(uint64_t address);
    uint64_t fifo(uint64_t address);
    uint64_t plru(uint64_t address);
    typedef uint64_t (Cache::* rptr)(uint64_t address); // Use this to refer to the previous possible replacement methods
    rptr replacement;
    
    uint64_t non_inclusive(uint64_t address);
    uint64_t inclusive(uint64_t address);
    uint64_t exclusive(uint64_t address);
    typedef uint64_t (Cache::* iptr)(uint64_t address); // Use this to refer to the previous possible inclusion propreties
    iptr inclusion;
    
    void calculate_tag_size();
    void calculate_index_size();
    void updateTime(uint64_t);
    
public:
    Cache (uint64_t block_size, uint64_t size, uint64_t associativity, uint16_t replacement_policy, uint16_t inclusion_proprety,Measurements *stat_keeper);
    Cache ();
    int print_propreties();
    void print_cache();
    uint64_t read(uint64_t address);
    void write(uint64_t address, bool dirty);
    void set_dirty_bit(uint64_t address);
    void unset_dirty_bit(uint64_t address);
    void evict(uint64_t address, uint64_t assoc);
    bool hasBlock (uint64_t address);
    void fixL2(uint64_t address);
    bool isDirty(uint64_t address);
    void specialEvict(uint64_t address);
    
    void set_next_cache(Cache*);
    void set_previous_cache(Cache*);
    void set_address_size(uint64_t);
};

#endif /* defined(__Project_1__cache__) */
