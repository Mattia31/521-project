//
//  utils.h
//  Project 1
//
//  Created by Mattia Muller on 2/11/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#ifndef __Project_1__utils__
#define __Project_1__utils__

#include <stdint.h>
#include <iostream>
#include <fstream>

int new_trace_file(std::string trace_file );
int append_trace_to_file (char mode, uint64_t address, std::string trace_file);

#define nullptr NULL

#endif /* defined(__Project_1__utils__) */
