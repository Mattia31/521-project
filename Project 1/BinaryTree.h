//
//  BinaryTree.h
//  Project 1
//
//  Created by Mattia Muller on 3/5/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#ifndef Project_1_BinaryTree_h
#define Project_1_BinaryTree_h

#include <stdint.h>
#include <iostream>
#include <iomanip>
#include <queue>
#include <utility>   // for std::pair
#include <math.h>
#include "utils.h"

struct node
{
    uint16_t value;
    uint16_t lru;
    uint16_t invalid;
    uint16_t assoc;
    node *zero;
    node *one;
};

class BinaryTree
{
public:
    BinaryTree();
    BinaryTree(uint16_t associativity_value);
    ~BinaryTree();
    
    void insert(int key);
    node *search(int key);
    void destroy_tree();
    uint16_t getPLRU();
    uint16_t setPLRU();
    void set_depth(uint16_t associativity_value);
    void print(int indent);
    void setMRU(int assoc);
    
private:
    void destroy_tree_pvt(node *leaf);
    void insert_pvt(int key, node *leaf);
    node *search(int key, node *leaf);
    void create();
    uint16_t getKeyMax(uint16_t level);
    node* getPLRU_pvt(node *leaf);
    node* setPLRU_pvt(node *leaf);
    node *root;
    uint16_t associativity; // Is gonna be equal to the index bit size
    uint16_t last_direction;
    void postorder(node* p, int indent);
    void printBinaryTree(node *n);
    node* search_invalid(node *leaf);
    node* setMRU_pvt(int, int, node *leaf);

};



#endif
