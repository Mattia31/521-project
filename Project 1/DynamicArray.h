//
//  DynamicArray.h
//  Project 1
//
//  Created by Mattia Muller on 3/2/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#ifndef Project_1_DynamicArray_h
#define Project_1_DynamicArray_h
#include <vector>
#include <stdint.h>

template <typename T>
class DynamicArray
{
public:
    DynamicArray(){};
    
    DynamicArray(uint64_t rows, uint64_t cols): dArray(rows, std::vector<T>(cols)){}
    
    std::vector<T> & operator[](uint64_t i)
    {
        return dArray[i];
    }
    const std::vector<T> & operator[] (uint64_t i) const
    {
        return dArray[i];
    }
    void resize(uint64_t rows, uint64_t cols)//resize the two dimentional array .
    {
        dArray.resize(rows);
        for(uint64_t i = 0;i < rows;++i){
            dArray[i].resize(cols);
            for (uint64_t j = 0; j<cols; j++)
                dArray[i][j] = ~(0);
        }
    }
private:
    std::vector<std::vector<T> > dArray;
};

#endif
