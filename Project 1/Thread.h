//
//  Thread.h
//  Project 1
//
//  Created by Mattia Muller on 3/6/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#ifndef __Project_1__Thread__
#define __Project_1__Thread__

#define L1_CACHE  0
#define L2_CACHE  1

#include <stdint.h>
#include <iostream>
#include "utils.h"
#include "Cache.h"



class Thread{
    //private:
    
public:
    Thread();
    void setArguments(const char **argv);
    void setArgumentsTest(uint64_t BLOCKSIZE,
                          uint64_t L1_SIZE,
                          uint64_t L1_ASSOC,
                          uint64_t L2_SIZE,
                          uint64_t L2_ASSOC,
                          uint16_t REPLACEMENT_POLICY,
                          uint16_t INCLUSION_PROPERTY,
                          std::string trace_file);
    void start();
    
    Cache* L1;
    Cache* L2;
    Measurements *stats[2];
    
    bool print, unit_test, validation;
};



#endif /* defined(__Project_1__Thread__) */
