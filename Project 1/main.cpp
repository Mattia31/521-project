//
//  main.cpp
//  Project 1
//
//  Created by Mattia Muller on 2/11/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#include "main.h"

int main(int argc, const char * argv[]) {
    
    if(argc < 9){
        std::cout << "There are not enough parameters please use the following format: \n";
        std::cout << " <BLOCKSIZE> <L1_SIZE> <L1_ASSOC> <L2_SIZE> <L2_ASSOC> <REPLACEMENT_POLICY> <INCLUSION_PROPERTY> <trace_file>\n";
        std::cout << "Exiting";
        exit(0);
    }
    
    Thread* t = new Thread();
    t->setArguments(argv);
    t->start();
    
      return 0;
}


