//
//  cache.cpp
//  Project 1
//
//  Created by Mattia Muller on 2/12/15.
//  Copyright (c) 2015 Mattia Muller. All rights reserved.
//

#include "Cache.h"

Cache::Cache (){
    p.block_size = 0;
    p.size = 0;
    p.associativity = 0;
    p.replacement_policy = LRU;
    p.inclusion_proprety = NON_INCLUSIVE;
    this->stat_keeper = new Measurements();
    time = 0;
}

Cache::Cache (uint64_t block_size, uint64_t size, uint64_t associativity, uint16_t replacement_policy, uint16_t inclusion_proprety, Measurements *stat_keeper) {
    time = 0;
    p.block_size = block_size;
    p.size = size;
    p.associativity = associativity;
    p.replacement_policy = replacement_policy;
    p.inclusion_proprety = inclusion_proprety;
    this->stat_keeper = stat_keeper;
    
    switch (p.inclusion_proprety) {
        case NON_INCLUSIVE:
            //uint64_t result = (*inclusion)(uint64_t 3);
            inclusion = &Cache::non_inclusive;
            break;
        case INCLUSIVE:
            inclusion = &Cache::inclusive;
            break;
        case EXCLUSIVE:
            inclusion = &Cache::exclusive;
            break;
        default:
            break;
    }
    
    switch (p.replacement_policy) {
        case LRU:
            //uint64_t result = (*inclusion)(uint64_t 3);
            replacement = &Cache::lru;
            break;
        case FIFO:
            replacement = &Cache::fifo;
            break;
        case PLRU:
            replacement = &Cache::plru;
            break;
        default:
            break;
    }
    
}

int Cache::print_propreties(){
    std::cout << "block_size: " << std::dec << p.block_size  << " size: " <<  p.size  << " associativity: " << p.associativity  << "\n";
    std::cout << "replacement_policy: " <<  p.replacement_policy  << " inclusion_proprety: " <<  p.inclusion_proprety  << "\n";
    std::cout << "previousCache: " <<  p.previousCache  << " nextCache: " <<  p.nextCache  << "\n";
    return 0;
};

void Cache::print_cache(){
    std::cout << "cache_table contains:";
    std::cout << std::endl;
}


// public
uint64_t Cache::read(uint64_t address){ // returns the address for upper level cache
    uint64_t has_empty_spot = 0;
    // 1 - Hit or miss ?
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size); // Shifting to have a maximum number = to max table
    uint64_t tag = (uint64_t) (address & tag_mask); // No shiftig required
    
    
    uint64_t tag_test = index << p.offset_size;
    tag_test |= tag; // we do not care about the offset blocks
    if (tag_test == 0xaeb2d8908840) {
        int caca = 1;
    }
    
    stat_keeper->read();
    last_state = READ;
    for (uint64_t i = 0; i<p.associativity; i++){
        if (cache_table[index][i] == tag){
            updateTime(address); // Hit
            CALL_MEMBER_FN(inclusion)(address); // Do the inclusion stuff
            return address;
        }
        if (cache_table[index][i] == (MAX_INT_64) && has_empty_spot == 0) {
            has_empty_spot = i+1;
        }
    }
    // Miss
    stat_keeper->read_miss();
    last_state = READ_MISS;
    // Check if we are doing exclusive stuff if so if we missed read we do not always need to bring the stuff back up
    if  (p.inclusion_proprety == EXCLUSIVE && p.previousCache != nullptr){
        if(p.previousCache->hasBlock(address))
            return tag;
    }
    
    // Do we have an empty spot?
    if (has_empty_spot) {
        cache_table[index][has_empty_spot-1] = tag;
        updateTime(address);
        // Check in next cache
        if (p.nextCache != nullptr){
            p.nextCache->read(address);
        }
        CALL_MEMBER_FN(inclusion)(address);
        return tag;
    }
    // There are no empty spots... We have to replace a previous entry
    // let's evict and get the associativity of the evicted block according to replacement policy
    // Let's ask the replacement policy wich one
    uint64_t assoc_to_replace = CALL_MEMBER_FN(replacement)(address);
    // Can we replace this block or not?
    evict(address, assoc_to_replace);
    cache_table[index][assoc_to_replace] = tag;
    updateTime(address);
    // Check in next cache
    if (p.nextCache != nullptr){
        p.nextCache->read(address);
    }
    CALL_MEMBER_FN(inclusion)(address);
    return tag;
}

bool Cache::isDirty(uint64_t address){
    // this should always hit, in fact we just placed it...
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t tag = (uint64_t) (address & tag_mask);
    for (uint64_t i = 0; i<p.associativity; i++){
        if (cache_table[index][i] == tag){
            if(dirty_table[index][i] == 1)
                return true;
            else
                return false;
        }
    }
    return false;
}


bool Cache::hasBlock (uint64_t address){
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t tag = (uint64_t) (address & tag_mask);
    
    for (uint64_t i = 0; i<p.associativity; i++){
        if (cache_table[index][i] == tag){
            return true;
        }
    }
    
    return false;
    
}

void Cache::write(uint64_t address, bool dirty){
    uint64_t has_empty_spot = 0;
    // 1 - Hit or miss ?
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t tag = (uint64_t) (address & tag_mask);
    stat_keeper->write();
    last_state = WRITE;
    
    
    uint64_t tag_test = index << p.offset_size;
    tag_test |= tag; // we do not care about the offset blocks
    if (tag_test == 0xaeb2d8908840) {
        int caca = 1;
    }
    
    for (uint64_t i = 0; i<p.associativity; i++){
        if (cache_table[index][i] == tag){
            updateTime(address);  // Hit
            if (dirty) {
                dirty_table[index][i] = 1;
            }
            CALL_MEMBER_FN(inclusion)(address); // Do the inclusion stuff
            return;
        }
        if (cache_table[index][i] == MAX_INT_64 && has_empty_spot == 0) {
            has_empty_spot = i+1;
        }
    }
    // Miss
    stat_keeper->write_miss();
    last_state = WRITE_MISS;
    // Check if we are doing exclusive stuff if so if we missed read we do not always need to bring the stuff back up
    if  (p.inclusion_proprety == EXCLUSIVE && p.previousCache != nullptr){
        if(p.previousCache->hasBlock(address))
            return;
    }
    
    // Do we have an empty spot?
    if (has_empty_spot) {
        cache_table[index][has_empty_spot-1] = tag;
        updateTime(address);
        if (dirty) {
            dirty_table[index][has_empty_spot-1] = 1;
        }        // Check in next cache
        if (p.nextCache != nullptr){
            p.nextCache->read(address);
        }
        CALL_MEMBER_FN(inclusion)(address);
        return;
    }
    // There are no empty spots... We have to replace a previous entry
    // let's evict and get the associativity of the evicted block according to replacement policy
    // Let's ask the replacement policy wich one
    uint64_t assoc_to_replace = CALL_MEMBER_FN(replacement)(address);
    evict(address, assoc_to_replace);
    cache_table[index][assoc_to_replace] = tag;
    if (dirty) {
        dirty_table[index][assoc_to_replace] = 1;
    }
    updateTime(address);
    // Check in next cache
    if (p.nextCache != nullptr){
        p.nextCache->read(address);
    }
    CALL_MEMBER_FN(inclusion)(address);
    return;
}

void Cache::set_dirty_bit(uint64_t address){
    // this should always hit, in fact we just placed it...
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t tag = (uint64_t) (address & tag_mask);
    for (uint64_t i = 0; i<p.associativity; i++){
        if (cache_table[index][i] == tag){
            dirty_table[index][i] = 1;
        }
    }
}

void Cache::specialEvict(uint64_t address){
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t tag = (uint64_t) (address & tag_mask);
    for (uint64_t i = 0; i<p.associativity; i++){
        if (cache_table[index][i] == tag){
            cache_table[index][i] = MAX_INT_64;
            if (p.replacement_policy == LRU || p.replacement_policy == FIFO)
                time_table[index][i] = MAX_INT_64;
            dirty_table[index][i] = MAX_INT_64;
        }
    }
}


void Cache::evict(uint64_t address, uint64_t assoc){
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t tag = (uint64_t) (address & tag_mask);
    if (assoc == MAX_INT_64){ // We just wanna get rid of the address in the cache
        for (uint64_t i = 0; i<p.associativity; i++){
            if (cache_table[index][i] == tag){
                uint64_t old_tag = cache_table[index][assoc];
                uint64_t old_address = index << p.offset_size;
                old_address |= old_tag; // we do not care about the offset blocks
                cache_table[index][i] = MAX_INT_64;
                if (p.replacement_policy == LRU || p.replacement_policy == FIFO)
                    time_table[index][i] = MAX_INT_64;
                if (dirty_table[index][i] == 1){ // if we do have a dirty bit and we are in L1 we have to write back to L2 or memory
                    if ((last_state == READ || last_state == WRITE)){
                        // We are in the case where we need to just set a dirty bit back to L1 if it is
                        if(p.previousCache != nullptr && p.inclusion_proprety == EXCLUSIVE){
                            p.previousCache->set_dirty_bit(address);
                            dirty_table[index][i] = MAX_INT_64;
                            return;
                        }
                    }
                    if(p.previousCache != nullptr && p.inclusion_proprety == INCLUSIVE && p.previousCache->isDirty(old_address)){
                        dirty_table[index][i] = MAX_INT_64;
                        return;
                    }
                    
                    
                    stat_keeper->write_back();
                    dirty_table[index][i] = MAX_INT_64;
                    if (p.nextCache != nullptr){
                        p.nextCache->write(old_address, true); //we want to write the old address to the cache not the current one so let's pass the tag plus index
                    }
                }  else if (p.inclusion_proprety == EXCLUSIVE){
                    if (p.nextCache != nullptr){
                        p.nextCache->write(old_address, false); //we want to write the old address to the cache not the current one so let's pass the tag plus index
                        // This one is NOT dirty so we need to make sure that bit will not be set the dirty bit we just set
                    }
                }
            }
        }
    }else {
        uint64_t old_tag = cache_table[index][assoc];
        uint64_t old_address = index << p.offset_size;
        old_address |= old_tag; // we do not care about the offset blocks
        cache_table[index][assoc] = MAX_INT_64;
        if (p.replacement_policy == LRU || p.replacement_policy == FIFO)
            time_table[index][assoc] = MAX_INT_64;
        if (dirty_table[index][assoc] == 1){ // if we do have a dirty bit and we are in L1 we have to write back to L2 or memory
            if ((last_state == READ || last_state == WRITE)){
                // We are in the case where we need to just set a dirty bit back to L1 if it is
                if(p.previousCache != nullptr && p.inclusion_proprety == EXCLUSIVE){
                    p.previousCache->set_dirty_bit(address);
                    dirty_table[index][assoc] = MAX_INT_64;
                    return;
                }
            }
            if(p.previousCache != nullptr && p.inclusion_proprety == INCLUSIVE && p.previousCache->isDirty(old_address)){
                dirty_table[index][assoc] = MAX_INT_64;
                return;
            }
            
            stat_keeper->write_back();
            dirty_table[index][assoc] = MAX_INT_64;
            if (p.nextCache != nullptr){
                p.nextCache->write(old_address, true); //we want to write the old address to the cache not the current one so let's pass the tag plus index
            }
        } else if (p.inclusion_proprety == EXCLUSIVE){
            if (p.nextCache != nullptr){
                p.nextCache->write(old_address, false); //we want to write the old address to the cache not the current one so let's pass the tag plus index
            }
        }
    }
}

// Private

void Cache::calculate_tag_size(){
    // # tag bits = 32 - # index bits - # block offset bits
    p.offset_size = (uint16_t) ceil(log2(p.block_size));
    p.tag_size = p.address_size - log2(p.index_size) - p.offset_size ;
}

void Cache::calculate_index_size(){
    // log2( #sets); #sets = size * cache size / associativity
    p.index_size = (uint64_t) ceil(((float_t)((float_t)p.size / p.block_size)/p.associativity));
}

uint64_t Cache::lru(uint64_t address){
    // Find lowest time in the set of associative data
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t i_end = 0;
    uint64_t small[p.associativity];
    for (uint64_t i = 0; i<p.associativity; i++)
        small[i]=i;
    std::pair<uint64_t, uint64_t> AB[64];
    //copy into pairs
    //Note that A values are put in "first" this is very important
    for( size_t i = 0; i<p.associativity; ++i)
    {
        AB[i].second  = small[i];
        AB[i].first = time_table[index][i];
    }
    std::sort(AB, AB+p.associativity);
    
    //Place back into arrays
    for( size_t i = 0; i<p.associativity; ++i)
    {
        small[i] = AB[i].second;
    }
    return small[i_end];
}

uint64_t Cache::fifo(uint64_t address){
    // Find lowest time in the set of associative data
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t small = 0;
    for (uint64_t i = 0; i<p.associativity; i++){
        if (time_table[index][small] > time_table[index][i]){
            small = i;
        }
    }
    // before returning we clear the time table for that cell
    cache_table[index][small] = MAX_INT_64;
    return small;
}

uint64_t Cache::plru(uint64_t address){
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
    uint64_t small = 0;
    if(tree_table[index] == nullptr){
        tree_table[index] = new BinaryTree(p.associativity);
    }
    small = tree_table[index]->getPLRU();
    //std::cout << small << '\n';
    return small;
}

uint64_t Cache::non_inclusive(uint64_t address){
    // With a non inclusive cache we do not really care; what is there is there
    return 0;
}

uint64_t Cache::inclusive(uint64_t address){
    bool L1_cache_has_block = false;
    bool L2_cache_has_block = false;
    // If we evict a block in L2 that is present in L1 we need to evict it from L1
    // Make sure this is an L2 cache
    if (p.previousCache != nullptr) { // This has to be an L2 for our implementation
        // do we have the block ?
        L2_cache_has_block = hasBlock(address);
        L1_cache_has_block = p.previousCache->hasBlock(address);
        // This inclusion proprety is a problem only when we evict so let's assume that only evict can call it
        // in this case we could say that if we evict a block from L1 or L2 we have to evict it from the other cache
        // however L2 is bigger than L1 so there could be a block in L2 but not in L1 so if the block is present is evicted from L2
        // we have to evict it from L1
        if (L1_cache_has_block != L2_cache_has_block && L1_cache_has_block){
            p.previousCache->specialEvict(address); // Back invalidation
            stat_keeper->back_inval();
        }
    }
    
    if (p.nextCache != nullptr) { // This has to be an L1 for our implementation
        // do we have the block ?
        L2_cache_has_block = p.nextCache->hasBlock(address);
        L1_cache_has_block = hasBlock(address);
        if (L1_cache_has_block != L2_cache_has_block && L1_cache_has_block){
            p.nextCache->read(address);
        }
    }
    
    if(p.previousCache != nullptr && !p.previousCache->hasBlock(address)){
        return 0 ; // skip no write to memmory needed
    }
    return 0;
}

void Cache::fixL2(uint64_t address){
    // 1 - Hit or miss ?
    uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size); // Shifting to have a maximum number = to max table
    uint64_t tag = (uint64_t) (address & tag_mask); // No shiftig required
    uint64_t assoc_to_replace = CALL_MEMBER_FN(replacement)(address);
    cache_table[index][assoc_to_replace] = tag;
    dirty_table[index][assoc_to_replace] = 1;
    updateTime(address);
}

uint64_t Cache::exclusive(uint64_t address){
    bool L1_cache_has_block = false;
    // If we have the same block in L2 we need to evict it from there
    // Make sure this is an L2 cache
    if (p.previousCache != nullptr) { // This has to be an L2 for our implementation
        L1_cache_has_block = p.previousCache->hasBlock(address);
        if (L1_cache_has_block){ // this means we need to evict the block from the other cache
            evict(address, MAX_INT_64); // L1 has it let's get rid of it
        }
    }
    return 0;
}



void Cache::set_next_cache(Cache* next){
    p.nextCache = next;
    my_cache_name = "L1";
}

void Cache::set_previous_cache(Cache* prev){
    p.previousCache = prev;
    my_cache_name = "L2";
}

void Cache::set_address_size(uint64_t address){
    std::stringstream ss;
    std::string temp;
    ss << std::hex << address;
    temp = ss.str();
    p.address_size = temp.length()*4;
    
    // Init the cache_tables
    // We do it here as this infromation is required
    calculate_index_size();
    calculate_tag_size(); // Do not call before index size calculation
    cache_table.resize(p.index_size, p.associativity);
    dirty_table.resize(p.index_size, p.associativity);
    if (p.replacement_policy == LRU || p.replacement_policy == FIFO)
        time_table.resize(p.index_size, p.associativity);
    if (p.replacement_policy == PLRU)
        tree_table.resize(p.index_size);
    // Usage cache_table[0][0]=5;
    tag_mask = ~(0) << (p.address_size - p.tag_size);//0x1111110000 with the ones being the size of the tag
    index_mask = ~(0);
    index_mask = index_mask << p.offset_size;
    index_mask = ~tag_mask & index_mask; // 0x0000_1111_0000 = 0x1111_1111_0000 AND ~(0x1111_0000_0000)
    
    if(p.nextCache != nullptr) // Set L2
        p.nextCache->set_address_size(address);
}

void Cache::updateTime(uint64_t address){
    switch (p.replacement_policy) {
        case LRU:
        {   // this should always hit, in fact we just placed it...
            uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
            uint64_t tag = (uint64_t) (address & tag_mask);
            for (uint64_t i = 0; i<p.associativity; i++){
                if (cache_table[index][i] == tag){
                    time_table[index][i] = time++;
                }
            }
        }
            break;
        case FIFO:
        {
            if (last_state == READ_MISS || last_state == WRITE_MISS){// Only add a timestamp if we missed
                uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
                uint64_t tag = (uint64_t) (address & tag_mask);
                for (uint64_t i = 0; i<p.associativity; i++){
                    if (cache_table[index][i] == tag){ // find the cell index and associativity value
                        time_table[index][i] = time++;
                    }
                }
            }
        }
            break;
        case PLRU:
        {
            uint64_t index = (uint64_t) ((address & index_mask) >> p.offset_size);
            uint64_t tag = (uint64_t) (address & tag_mask);
            for (uint64_t i = 0; i<p.associativity; i++){
                if (cache_table[index][i] == tag){ // find the cell index and associativity value
                    if(tree_table[index] == nullptr){
                        tree_table[index] = new BinaryTree(p.associativity);
                    }
                    tree_table[index]->setMRU((int)i);
                }
            }
            
        }
            break;
        default:
            break;
    }
}





